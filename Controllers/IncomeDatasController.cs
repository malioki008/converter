﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Converter.Data;
using Converter.Models.Company;
using Converter.Models.Company.Electro;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using Converter.Models.Actions;
using Microsoft.AspNetCore.Mvc.Formatters;
using Converter.Models.Company.JCH;

namespace Converter.Controllers
{
    public class IncomeDatasController : Controller
    {
        private readonly ConverterContext _context;

        public IncomeDatasController(ConverterContext context)
        {
            _context = context;
        }

        // GET: IncomeDatas
        public async Task<IActionResult> Index()
        {
            List<IncomeData> incomeDatas = new List<IncomeData>(1);
            incomeDatas.Add(new IncomeData() { Id = 1, Provider=1, Counter = "234", Account = "34-135623",  PaySumm = 323.00M });
            incomeDatas.Add(new IncomeData() { Id = 2, Provider = 2, Account = "34-135623",  PaySumm = 323.00M });
            incomeDatas.Add(new IncomeData() { Id =3, Provider = 3, Account = "34-135623", PaySumm = 323.00M });
            incomeDatas.Add(new IncomeData() { Id =4, Provider = 4, Account = "34-135623", PaySumm = 323.00M });
            return View(incomeDatas);
        }

        public IActionResult Convert(int provider, string account, string count, Counter counter, decimal paySumm)
        {
            if (provider != null)
            {
                IncomeData incomeData = new IncomeData() { Provider = provider, Counter= count, Account = account, PaySumm = paySumm };
                Counter counter1 = new Counter() { Type = 1, Value = "323.43" };
                Counter counter2 = new Counter() { Type = 2, Value = "234.56" };
                Counter[] counters = new Counter[2] { counter1, counter2 };
                switch (provider)
                {
                    case (1):
                        {
                            GasMyas list = new GasMyas("1mbank", "123456", incomeData);
                            return Content(list.SerializeJSON(list));                           
                        }
                    case (2):
                        {
                            Vodostok list = new Vodostok("2.0", "fpay", "1mbank", "33333", incomeData, counters);
                            return Content(list.SerializeJSON(list));
                        }
                    case (3):
                        {
                            Models.Company.JCH.xml incomeXml = new Models.Company.JCH.xml(incomeData);
                            return Content(incomeXml.SerializeXML(incomeXml));
                        }
                    case (4):
                        {
                            Models.Company.Electro.xml incomeXml = new Models.Company.Electro.xml("pas", "11111", incomeData, counters);                            
                            return Content(incomeXml.SerializeXML(incomeXml));
                        }
                    default:
                        {
                            return null;
                        }
                }
            }
            return null;
        }
        private bool IncomeDataExists(int id)
        {
            return _context.IncomeData.Any(e => e.Id == id);
        }
    }
}
