﻿using Converter.Models.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace Converter.Models.Actions
{
    public static class CompanyActions
    {
        public static string SerializeJSON<T>(this IAction<CanSerialize> action, T incomeData)
        {
            return JsonSerializer.Serialize(incomeData);
        }
        public static T DeserializeJSON<T>(this IAction<CanSerialize> action, string incomeData)
        {
            return JsonSerializer.Deserialize<T>(incomeData);
        }
        public static string SerializeXML<T>(this IAction<CanSerialize> action, T incomeData)
        {

            XmlSerializer xsSubmit = new XmlSerializer(incomeData.GetType());
            var xml = "";

            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, incomeData);
                    xml = sww.ToString(); // Your XML
                }
            }
            return xml;
        }
        public static T DeserializeXML<T>(this IAction<CanSerialize> action, string incomeData)
        {

            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (TextReader reader = new StringReader(incomeData))
            {
                T result = (T)serializer.Deserialize(reader);
                return result;
            }
        }

    }
}
