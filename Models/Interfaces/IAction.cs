﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Converter.Models.Interfaces
{
    public interface IAction<T> where T: Action 
    {
    }
}
