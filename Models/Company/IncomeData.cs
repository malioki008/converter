﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Converter.Models.Company
{
    public class IncomeData
    {
        public int Id { get; set; }
        public int Provider { get; set; } //Тип поставщика
        public virtual string Counter { get; set; } //Счёт
        public virtual string Account { get; set; } //Номер лицевого счёта
        public virtual decimal PaySumm { get; set; }  //Сумма платежа
    }
}
