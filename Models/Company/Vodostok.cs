﻿using Converter.Models.Actions;
using Converter.Models.Company;
using Converter.Models.Company.Electro;
using Converter.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace Converter.Models.Company
{
    public class Vodostok : IAction<CanSerialize>, IAction<CanDeserialize>
    {
        public string jsontpc { get; set; }
        public string method { get; set; }
        public Params Params { get; set; }
        public Guid id { get; set; }
        public Vodostok(){}
        public Vodostok(string jsontpc, string method, string login, string pass, IncomeData income, Counter []counters)
        {
            this.jsontpc = jsontpc;
            this.method = method;
            this.Params = new Params();
            this.Params.session = new Session();
            this.Params.session.login = login;
            this.Params.session.pass = pass;
            this.Params.data = new data();
            this.Params.data.account = income.Account;
            if (counters != null)
                this.Params.data.counters = counters;            
            this.id = Guid.NewGuid();
        }
    }
    public class Params
    {
        public Session session { get; set; }
        public data data { get; set; }
    }
    public class Session
    {
        public string login { get; set; }
        public string pass { get; set; }
    }

    public class data
    {
        public string account { get; set; }
        public Counter []counters { get; set; }
    }
}
