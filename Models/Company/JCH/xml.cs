﻿using Converter.Models.Actions;
using Converter.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Converter.Models.Company.JCH
{
    public class xml : IAction<CanSerialize>, IAction<CanDeserialize>
    {
        public string dogovor { get; set; }
        public DateTime date { get; set; }
        public decimal summa { get; set; }
        public xml() { }
        public xml(IncomeData date)
        {
            this.dogovor = date.Account;
            this.date = DateTime.Now;
            this.summa = date.PaySumm;
        }
    }
}
