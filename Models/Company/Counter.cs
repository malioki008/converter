﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Converter.Models.Company
{
    public class Counter
    {
        public int Type { get; set; } //Тип счётчика
        public string Value { get; set; } //Показатели счётчика
    }
}
