﻿using Converter.Models.Actions;
using Converter.Models.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Converter.Models.Company
{
    public class ElectroWide : IAction<CanSerialize>, IAction<CanDeserialize>
    {
        [FromForm(Name="c")]
        public List<Counter> cnt { get; set; }

    }
}
