﻿using Converter.Models.Actions;
using Converter.Models.Interfaces;
using System.Collections.Generic;

namespace Converter.Models.Company
{
    public class GasMyas: IAction<CanSerialize>, IAction<CanDeserialize>
    {
        public string login { get; set; }
        public string parol { get; set; }
        public List<IncomeData> data { get; set; }
        public GasMyas() { }
        public GasMyas(string login, string parol, IncomeData income)
        {
            
            this.login = login;
            this.parol = parol;
            this.data = new List<IncomeData>(1){ income };
        }
    }
}
