﻿using Converter.Models.Actions;
using Converter.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Converter.Models.Company.Electro
{
    public class xml : IAction<CanSerialize>, IAction<CanDeserialize>
    {
        [XmlElement]
        public c ses { get; set; }
        [XmlArray("s")]
        public List<c> counters { get; set; }
        [XmlElement(ElementName ="c")]
        public a c { get; set; }
        public xml() { }
        public xml(string login, string pass, IncomeData incomeData, Counter []counters)
        {
            this.ses = new c();
            this.ses.n = "ses";
            this.ses.a = new List<a>()
            {
                new a()
                {
                    n="log",
                    text = login
                },
                new a()
                {
                    n="pas",
                    text = pass
                },
                new a()
                {
                    n="acc",
                    text = incomeData.Account
                },
            };
            this.counters = new List<c>();
            if( counters != null )
            {
                foreach(Counter counter in counters)
                {
                    this.counters.Add(new Electro.c() { n = "cnt", a = new List<a>() { new a() {n="typ",text=counter.Type.ToString() }, new a() { n = "val", text = counter.Value } } });
                }
            }
            this.c = new a() { n = "sum", text = incomeData.PaySumm.ToString() };
        }
    }
}
