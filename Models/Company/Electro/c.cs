﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Converter.Models.Company.Electro
{
    public class c
    {
        [XmlAttribute]
        public string n { get; set; }
        [XmlElement]
        public List<a> a { get; set; }
    }
}
