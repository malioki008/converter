﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Converter.Models;

namespace Converter.Data
{
    public class ConverterContext : DbContext
    {
        public ConverterContext (DbContextOptions<ConverterContext> options)
            : base(options)
        {
        }

        public DbSet<Converter.Models.Company.IncomeData> IncomeData { get; set; }

        public DbSet<Converter.Models.Company.Counter> Counter { get; set; }
    }
}
